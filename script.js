/* 
1. Прототипне наслідування в Javascript дозволяє дочірньому елементу мати всі властивості та методи, які притаманні
батьківському елементу без необхідності прописувати їх (якщо інше явно не прописано для дочірнього елемента).
2. Super() у конструкторі класу-нащадка необхідний аби знайти батьківський прототип та його методи. 
*/

class Employee {
    constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get someName (){return this.name;}
  get someAge (){return this.age;}
  get someSalary (){return this.salary;}

  set someName (newName){this.name = newName;}
  set someAge (newAge){this.age = newAge;}
  set someSalary (newSalary){this.salary = newSalary;}
}

class Programmer extends Employee {
  constructor (name, age, salary, lang){
  super (name, age, salary);
  this.lang = lang;};
  get triple (){return this.someSalary * 3}
}

const ivan = new Programmer ('Ivan', 24, 1200, 'JS')
const iryna = new Programmer ('Iryna', 33, 900, 'UKR')
const nata = new Programmer ('Nata', 34, 5200, 'ENG')

console.log(ivan.triple)
console.log(iryna)
console.log(nata)